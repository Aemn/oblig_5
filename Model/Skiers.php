<?php



class Skier {
	public $userName;
	public $firstName;
	public $lastName;
	public $yearOfBirth;

	public function __construct($userName, $firstName, $lastName, $yearOfBirth)  
    {  
        $this->userName = $userName;
        $this->firstName = $firstName;
	    $this->lastName = $lastName;
	    $this->yearOfBirth = $yearOfBirth;
    } 
}


class Clubs {
	public $id;
	public $name;
	public $city;
	public $county;

	public function __construct($id, $name, $city, $county)  
    {  
        $this->id = $id;
        $this->name = $name;
	    $this->city = $city;
	    $this->county = $county; 
    } 
}

class Season {
	public $fallYear;
	public $userName;
	public $clubId;
	public $totalDistance; 

	public function __construct($fallYear, $clubId, $userName, $totalDistance)  
    {  
        $this->fallYear = $fallYear;
        $this->clubId = $clubId;
	    $this->userName = $userName;
	    $this->totalDistance = $totalDistance; 
    } 
}


?>