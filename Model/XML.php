<?php
include_once('Skiers.php'); 

class xmlDom{

	protected $db = null; 
	protected $doc; 

	public function __construct(){
			$this->$doc = new DOMDocument(); 
			$this->$doc->load('SkierLogs.xml');
		}; 


	public function getSkiers() {
		$xpath = new DOMXPath($this->$doc);
		$SkiEntriesArray = array();  
		$query = '//SkierLogs/Skiers/Skier';
		$SkiEntries = $xpath->query($query); 

		foreach $SkiEntries as $SkiEntry {
			$userName = $SkiEntry->getAttribute('userName'); 
			$firstName = $SkiEntry->getElementsByTagName('FirstName')->item(0)->textcontent; 
			$lastName = $SkiEntry->getElementsByTagName('LastName')->item(0)->textcontent; 
			$YearOfBirth = $SkiEntry->getElementsByTagName('YearOfBirth')->item(0)->textcontent; 
			$SkiEntriesArray[] = new Skier($userName,$firstName,$lastName,$YearOfBirth);		
		}

		return $SkiEntriesArray; 
	}





	public function getClubs() {
		$xpath = new DOMXPath($this->$doc);
		$ClubEntriesArray = array();  
		$query = '//SkierLogs/Clubs/Club';
		$ClubEntries = $xpath->query($query); 

		foreach $ClubEntries as $ClubEntry {
			$id = $ClubEntry->getAttribute('id'); 
			$name = $ClubEntry->getElementsByTagName('Name')->item(0)->textcontent; 
			$city = $ClubEntry->getElementsByTagName('City')->item(0)->textcontent; 
			$county = $ClubEntry->getElementsByTagName('County')->item(0)->textcontent; 
			$ClubEntriesArray[] = new Skier($userName,$firstName,$lastName,$YearOfBirth);		
		}

			return $ClubEntriesArray; 
		}



	public function getSeasons() {
		$xpath = new DOMXPath($this->$doc);
		$SeasonEntriesArray = array();  
		$SeasonEntries = $xpath->getElementsByTagName('Season');

		foreach $SeasonEntries as $SeasonEntry {
			$fallYear = $SeasonEntry->getAttribute('fallYear'); 
			$clubs = $SeasonEntry->getElementsByTagName('Skiers'); 
			foreach $clubs as $ClubEntry {
				$clubID = $ClubEntry->getAttribute('clubId');  
				$skiers = $ClubEntry->getElementsByTagName('Skier'); 
				foreach $skiers as $skierEntry{
					$userName = $skierEntry->getAttribute('userName');
					$entries = $skierEntry->getElementsByTagName('Entry'); 
					$totalDistance = 0;
					foreach $entries as $entry {
						$distance = $entry->getElementsByTagName('Distance')->item(0)->nodeValue;
						$totalDistance += $distance;
					}   
					
					$SeasonEntriesArray[] = new Season($fallYear, $clubID, $userName, $totalDistance);
				}
			}
		}
			return $SeasonEntriesArray;
	};

};

?>